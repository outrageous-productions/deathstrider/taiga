# Taiga

A weapon addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

An HMG with a 50 round 50BMG box, and a secondary 5 round 35mm cannon.

Firing the gun will fatigue you the longer you fire it. Higher strength will diminish the effect.

---

See [credits.txt](./credits.txt) for attributions.
